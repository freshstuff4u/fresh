Polyverse Music Plugin Bundle 2019 [WINDOWS]
Size 26 Mb

The Polyverse Plugin Bundle includes all three of Polyverse’s groundbreaking plugins, offering a full suite of cutting edge tools for audio creation, shaping and performance.

Create beautiful 4 part harmonies with Manipulator’s zero latency vocal processing, or use it to morph them into insane, spaced out sounds unlike any other. Then, add mesmerizing rhythmic patterns with Gatekeeper’s 8 independent envelopes and one-of-a-kind automation brushes. Or freeze any audio in real time with the original I Wish plugin! Build highly personalized and unique textures with micro-editing, or play your sound as a real-time wavetable synthesizer.

Get a powerhouse of creative expression and tap into Infected Mushroom’s tried and true studio secrets!

Polyverse Music is a company dedicated to collaborations with the most innovative musicians of our age to create musical instruments and tools for fellow musicians.

Polyverse Music Plugin Bundle 2019 Includes:
Comet v1.0 - Lush, versatile reverb.
Infected Mushroom Gatekeeper v1.2 - Precise volume modulation.
Infected Mushroom I Wish v1.0.1 - Pitch freezer.
Infected Mushroom Manipulator v1.0.3 - Extreme vocal transformer.
Infected Mushroom Wider v1.0 - Free stereo widener.

Comet is a powerful, lush sounding reverb with a uniquely rich and musical detune algorithm for creating incredible reverb textures. Gatekeeper is a cutting edge volume modulation tool with unbeatable versatility. Together, these tools form a groundbreaking combination of creative tools, even harnessing the ability to be connected together through their CV outputs. By routing Gatekeeper's CV output into Comet, you can create moving, evolving reverb sounds that will be truly unique to your productions.

Wider takes the massively popular stereo section from its sibling, Manipulator, and creates the illusion of an expanded stereo image of a mono signal to an awe-inspiring amount of width. However, Wider is a unique stereo plugin in the sense that it is completely “mono-compatible”, meaning that any signal that has been extended will always remain in phase with itself, even if summed to mono.

I Wish is a pitch freezer plug-in. It allows you to take any sound – a vocal recording, a drum beat or even noise, and instantly freeze it in time and pitch. The pitch-frozen audio can then be manipulated and modulated to create highly musical mind-bending effects and sounds that have never been heard before! Is it a synthesizer? is it an effect? It's both!

Take any sound in the world, freeze it in time and sculpt mind-bending sounds and music.

Through unique granular algorithms, Manipulator can dramatically alter the timbre and pitch of monophonic audio in new and unexplored ways. From subtly imposing pitch and harmonization, to a full-on sonic mangle that will leave you with a totally new sound, Manipulator is as versatile as it is creativity-inducing.

Gatekeeper is the cutting edge of volume modulation. Capable of sample-fast transitions and equipped with a unique variable smoothing algorithm, Gatekeeper allows for drastic, punchy gating and sequencing while keeping the output smooth. Breathe new life into your audio with the most basic, yet impactful way: silence.


https://freshstuff4you.com